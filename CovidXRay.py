#import neccessery libs
%matplotlib inline
import random
import torch
import torchvision
import os
from matplotlib import pyplot as plt

import shutil
import pandas as pd

import numpy as np

from PIL import Image

torch.manual_seed(0)
from torch.autograd import Variable

#split data to training and test
 classification_types = ['health', 'virus_pneu', 'covid']
 root_dir = 'Database'
 source_dirs = ['Health', 'Virus_pneu', 'COVID-19']

 if os.path.isdir(os.path.join(root_dir, source_dirs[1])):
     os.mkdir(os.path.join(root_dir, 'test'))

     for i, d in enumerate(source_dirs):
         os.rename(os.path.join(root_dir, d), os.path.join(root_dir, classification_types[i]))

     for c in classification_types:
         os.mkdir(os.path.join(root_dir, 'test', c))

     for c in classification_types:
         images = [x for x in os.listdir(os.path.join(root_dir, c)) if x.lower().endswith('png')]
         selected_images = random.sample(images, 30)
         for image in selected_images:
             source_path = os.path.join(root_dir, c, image)
             target_path = os.path.join(root_dir, 'test', c, image)
             shutil.move(source_path, target_path)

# custom dataset

class XRayData(torch.utils.data.Dataset):
    def __init__(self, data_location, remake):
        def img(classification_type):
            imgs = [x for x in os.listdir(data_location[classification_type]) if x[-3:].lower().endswith('png')]
            return imgs
        
        self.images = {}
        self.classification_types = ['health', 'virus_pneu', 'covid']
        
        for classification_type in self.classification_types:
            self.imgs[classification_type] = img(classification_type)
            
        self.data_location = data_location
        self.remake = remake
        
    
    def __len__(self):
        return sum([len(self.images[classification_type]) for classification_type in self.classification_types])
    
    
    def __getitem__(self, index):
        classification_type = random.choice(self.classification_types)
        index = index % len(self.images[classification_type])
        image_name = self.images[classification_type][index]
        image_path = os.path.join(self.data_location[classification_type], image_name)
        image = Image.open(image_path).convert('RGB')
        return self.remake(image), self.classification_types.index(classification_type)
        
# transform image (preprocessing)
train_transform = torchvision.transforms.Compose([
    torchvision.transforms.Resize(size = (224,224)),
    torchvision.transforms.RandomHorizontalFlip(),
    torchvision.transforms.ToTensor(),
    torchvision.transforms.Normalize(mean =[0.485,0.456,0.406],std=[0.229,0.224,0.225]),    
])
test_transform = torchvision.transforms.Compose([
    torchvision.transforms.Resize(size = (224,224)),
    torchvision.transforms.ToTensor(),
    torchvision.transforms.Normalize(mean =[0.485,0.456,0.406],std=[0.229,0.224,0.225]),    
])

#get DATA
train_dirs = {
    'health': 'Database/health',
    'virus_pneu': 'Database/virus_pneu',
    'covid': 'Database/covid'
}

train_dataset = XRayData(train_dirs,train_transform)

test_dirs = {
    'health': 'Database/test/health',
    'virus_pneu': 'Database/test/virus_pneu',
    'covid': 'Database/test/covid'
}

test_dataset = XRayData(test_dirs,test_transform)

batch_size = 6

dl_train = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
dl_test = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=True)

print('Number of training batches', len(dl_train))
print('Number of test batches', len(dl_test))

# show date using matplotlib
classification_types = train_dataset.classification_types


def show_images(images, labels, preds):
    plt.figure(figsize=(8, 4))
    for i, image in enumerate(images):
        plt.subplot(1, 6, i + 1, xticks=[], yticks=[])
        image = image.numpy().transpose((1, 2, 0))
        mean = np.array([0.485, 0.456, 0.406])
        std = np.array([0.229, 0.224, 0.225])
        image = image * std + mean
        image = np.clip(image, 0., 1.)
        plt.imshow(image)
        col = 'green'
        if preds[i] != labels[i]:
#             print("label",labels[i])
            col = 'red'
            
        plt.xlabel(f'{classification_types[int(labels[i].numpy())]}')
        plt.ylabel(f'{classification_types[int(preds[i].numpy())]}', color=col)
    plt.tight_layout()
    plt.show()
images,labels = next(iter(dl_train))
show_images(images,labels,labels)

#creating model
resnet18 = torchvision.models.resnet18(pretrained=True)
print(resnet18)
resnet18.fc = torch.nn.Linear(in_features=512, out_features=3)
loss_fn = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(resnet18.parameters(), lr=4e-5)
def show_preds():
    resnet18.eval()
    images, labels = next(iter(dl_test))
    outputs = resnet18(images)
    _, preds = torch.max(outputs, 1)
    show_images(images, labels, preds)

#train our model
def train(epochs):
    print('Starting training..')
    for e in range(0, epochs):
        print('='*20)
        print(f'Starting epoch {e + 1}/{epochs}')
        print('='*20)

        train_loss = 0.
        val_loss = 0.

        resnet18.train() 

        for train_step, (images, labels) in enumerate(dl_train):
            optimizer.zero_grad()
            outputs = resnet18(images)
            loss = loss_fn(outputs, labels)
            loss.backward()
            optimizer.step()
            train_loss += loss.item()
            if train_step % 20 == 0:
                accuracy = 0
                resnet18.eval() 

                for val_step, (images, labels) in enumerate(dl_test):
#                     print(type(images),images)
                    outputs = resnet18(images)
                    loss = loss_fn(outputs, labels)
                    val_loss += loss.item()

                    _, preds = torch.max(outputs, 1)
                    accuracy += sum((preds == labels).numpy())

                val_loss /= (val_step + 1)
                accuracy = accuracy/len(test_dataset)
                show_preds()

                resnet18.train()

                if accuracy >= 0.95:
                    break

        train_loss /= (train_step + 1)

    
with experiment.train():
    train(epochs=1)
    
# save model
torch.save(resnet18,'./model.pth')

#load model
model = torch.load('./model.pth')
model.eval()

#test our model
loader = torchvision.transforms.Compose([
    torchvision.transforms.Resize(size = (224,224)),
    torchvision.transforms.ToTensor(),
    torchvision.transforms.Normalize(mean =[0.485,0.456,0.406],std=[0.229,0.224,0.225]),    
])
def image_loader(image_name):
    image = Image.open(image_name).convert('RGB')
    image = loader(image).float()
    image = Variable(image, requires_grad=True)
    image = image.unsqueeze(0)
    return image

image = image_loader('health.png')

res_dict = {0:"Health",1:"Virus_pneu",2:"Covid"}
output = model(image)
_, preds = torch.max(output, 1)
print(res_dict[preds.tolist()[0]])
